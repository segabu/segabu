'use strict';

import React, { Component } from 'react';
import Button from 'react-native-button';
import NavigationBar from 'react-native-navbar';
import { Avatar, Card, COLOR } from 'react-native-material-design';
import * as Animatable from 'react-native-animatable';
import ProgressBar from 'react-native-progress/Bar';
//import Icon from 'react-native-vector-icons/FontAwesome';
import GameScene from './GameScene';
import SkillScene from './SkillScene';
import CVScene from './CVScene';
import ChatScene from './ChatScene';
import LogoutScene from './LogoutScene';
import HomeScene from './HomeScene';
import TutorialScene from './TutorialScene';
import SignUpScene from './SignUpScene';
import styles from './style';
import {
  AppRegistry,
  StyleSheet,
  TabBarIOS,
  Text,
  ListView,
  ScrollView,
  View,
  TextInput,
  Image,
  NavigatorIOS,
  TouchableHighlight
} from 'react-native';

function NotFoundError(e) {
    return e.statusCode === 404;
}

class HomeScene2  extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rovers: [],
      salasana: '',
      userName: '',
      textPW: '',
      lastName: '',
      firstName: '',
      alertText: 0
    }
  }

  Chatti() {
      this.props.navigator.push({
        barTintColor: '#b0ced8',
        tintColor: "#fff",
        titleTextColor: "#fff",
        component: HomeScene,
        leftButtonTitle: ' ',
        shadowHidden: true,
        rightButtonTitle: 'moi',
        onRightButtonPress: () => this.Chatti(),
        onLeftButtonPress: () => this.Takas(),
        title: 'Hire.me',
        passProps: {data: this.state.rovers}
      })
    }
    Takas() {
      this.setState({
        userName: '',
        textPW: '',
        alertText: 0
      })
      this.props.navigator.pop()
    }

  Latinki() {
    var myInit = {
      method: 'GET'
    };
    fetch('https://segabu2.herokuapp.com/contacts/user/' + this.state.userName, myInit)
        .then((result) => {
          return result.json();
        }).then((data) => {
            this.setState({
              rovers: data
            })
        }).then(() => {
          if(this.state.rovers !== null && this.state.rovers !== 'undefined') {
            this.Login()
          }
          else {
            this.setState({
             alertText: 1
            })
          }
        }).catch(NotFoundError, function(e) {
        reply(e);
    })      
  }

  Login() {
    //this.Latinki();
    //if (this.state.textPW==this.state.salasana && this.state.textEmail==this.state.email) {
    this.props.navigator.push({
        barTintColor: '#74A2D8',
        tintColor: "#fff",
        titleTextColor: "#fff",
        component: TutorialScene,
        leftButtonTitle: '< Back',
        shadowHidden: true,
        rightButtonTitle: 'Next >',
        onRightButtonPress: () => this.Chatti(),
        onLeftButtonPress: () => this.Takas(),
        title: '',
        translucent: false,
        passProps: {data: this.state.rovers}
    });
    //}
    //else {
      //this.setState({
      // alertText: 1
      //})
    //}
  }
  Cv() {
    this.props.navigator.push({
        navigationBarHidden: true,
        component: SignUpScene,
    });
  }

  render() {
    return (
      <Image source={require('./img/Background-01.png')} style={styles.backgroundImage}>
      <View style={{paddingTop: 50}}>
      <Text style={{backgroundColor: 'transparent', color: 'white', textAlign: 'center', fontSize: 32}}>Welcome to</Text>
      <Text style={{marginBottom: 20, backgroundColor: 'transparent', color: 'white', textAlign: 'center', fontSize: 42}}>hire.me</Text>
      <Text style={{marginBottom: 5, backgroundColor: 'transparent', color: 'white', textAlign: 'center', fontSize: 16}}>Please login to your</Text>
      <Text style={{marginBottom: 5, backgroundColor: 'transparent', color: 'white', textAlign: 'center', fontSize: 16}}>existing account or</Text>
      <Text style={{marginBottom: 20, backgroundColor: 'transparent', color: 'white', textAlign: 'center', fontSize: 16}}>create a new one</Text>
        <View style={{marginLeft:25, marginRight: 25, borderBottomWidth: 1, borderColor: 'white'}}>
          <TextInput placeholder="Username" placeholderTextColor="white" autoCapitalize={'none'} autoCorrect={false} style={{ height: 40, color: 'white', fontSize: 24}} onChangeText={(userName) => this.setState({userName})}
          value={this.state.userName}/>
        </View>
        <View style={{marginLeft:25, marginRight: 25, borderBottomWidth: 1, borderColor: 'white', marginTop: 25}}>
          <TextInput placeholder="Password" placeholderTextColor="white" secureTextEntry={true} style={{ height: 40, color: 'white', fontSize: 24}} onChangeText={(textPW) => this.setState({textPW})}
          value={this.state.textPW}/>
        </View>
        <View style={{alignItems: 'center'}}>
          <Text style={{color: 'red', backgroundColor: 'transparent', opacity: this.state.alertText}}>Wrong login credentials</Text>
          <Button 
            containerStyle={{padding: 10, marginTop: 5, height: 90, width: 90, overflow:'hidden', borderRadius: 70, borderWidth: 3, borderColor: 'white', backgroundColor: 'transparent'}}
            styleDisabled={{color: 'blue'}}
            onPress={() => this.Latinki()}>
            <Text style={{paddingTop: 15, paddingLeft: 8, marginLeft: -5, fontSize: 24, color: 'white'}}>Login</Text>
          </Button>
          <Button 
            containerStyle={{padding:10, marginTop: 5, height: 70, width: 70, overflow:'hidden', borderRadius: 35, borderWidth: 3, borderColor: 'white', backgroundColor: 'transparent'}}
            styleDisabled={{color: 'blue'}}
            onPress={() => this.Cv()}>
            <Text style={{paddingTop: 12, marginLeft: -5, fontSize: 14, color: 'white'}}>Sign up</Text>
          </Button>
        </View>
      </View>
      </Image>
    );
  }
};

var SeGaBu = React.createClass({
    render () {
        return (
            <NavigatorIOS ref="nav" style={styles.container} initialRoute={{
                component: HomeScene2,
                title: 'SeGaBu Jobs',
                tintColor: "#fff",
                titleTextColor: "#fff",
                barTintColor: '#9b59b6',
                navigationBarHidden: true,
                shadowHidden: true,
            }} />
        );
    }
});

AppRegistry.registerComponent('SeGaBu', () => SeGaBu);