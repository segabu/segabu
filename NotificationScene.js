'use strict';

import React, { Component } from 'react';
import Button from 'react-native-button';
import NavigationBar from 'react-native-navbar';
import reactRouter from 'react-router';
import { GiftedChat } from 'react-native-gifted-chat';
import { Router, Route, Link } from 'react-router';
import styles from './style';
import {
  AppRegistry,
  StyleSheet,
  TabBarIOS,
  Text,
  ListView,
  ScrollView,
  View,
  TextInput,
  Image,
  NavigatorIOS,
  TouchableHighlight
} from 'react-native';

class NotificationScene extends React.Component {
  constructor(props) {
    super(props);
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
        selectedTab: 'yellowTab',
        tyopaikka: '',
        jobs: [''],
        fieldAll: 'kaikki',
        dataSource: ds.cloneWithRows([
          'Notification A', 'Notification B', 'Notification C', 'Notification D', 'Notification E', 'Notification F', 'Notification G', 'Notification H'
        ])
    }
  }

  render() {
    return (
      <View style={{flex: 1, marginBottom: 50}}>
        <ListView
        dataSource={this.state.dataSource}
        renderRow={(rowData) => <Text style={{height: 50, paddingTop: 10, paddingBottom: 10,    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: '#d6d7da',}}>{rowData}</Text>}/>
      </View>
    );
  }
}
export default NotificationScene;





