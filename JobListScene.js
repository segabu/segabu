'use strict';

import React, { Component } from 'react';
import Button from 'react-native-button';
import NavigationBar from 'react-native-navbar';
import reactRouter from 'react-router';
import { Router, Route, Link } from 'react-router';
import GameScene from './GameScene';
import HomeScene from './HomeScene';
import CVScene from './CVScene';
import ChatScene from './ChatScene';
import LogoutScene from './LogoutScene';
import styles from './style';
import {
  AppRegistry,
  StyleSheet,
  TabBarIOS,
  Text,
  ListView,
  ScrollView,
  View,
  TextInput,
  Image,
  Dimensions,
  NavigatorIOS,
  SegmentedControlIOS,
  TouchableHighlight
} from 'react-native';

function NotFoundError(e) {
    return e.statusCode === 404;
}

class JobListScene extends Component {
  constructor(props) {
    super(props);
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
        selectedTab: 'yellowTab',
        tyopaikka: '',
        jobs: [],
        fieldAll: 'kaikki',
        dataSource: ds.cloneWithRows([
          ''
        ])
    }
  }

  loadJobs() {
    var myInit = {
      method: "GET"
    };
    fetch("http://gis.vantaa.fi/rest/tyopaikat/v1/" + this.state.fieldAll, myInit)
        .then((result) => {
          return result.json();
        }).then((data) => {
            this.setState({
              jobs: data
            })
        }).catch(NotFoundError, function(e) {
          reply(e);
        });
  }

  renderListView() {
    if (this.state.selectedIndex === 1) {
      return (
        <View style={styles.container}>
          {this.renderFeaturedCollectionsListView()}
        </View>
        )
    } else if (this.state.selectedIndex === 2) {
      return (
          this.renderAllCollectionsListView()
        )
    }
  }

  componentDidMount() {
    this.loadJobs();
  }

  renderAllCollectionsListView() {
    var ScreenWidth = Dimensions.get('window').width;
    return this.state.jobs.map(function(news, i){
      return(
        <View key={i} style={{width: ScreenWidth, minHeight: 80, paddingTop: 10, paddingBottom: 10, borderWidth: 0.5, borderColor: '#d6d7da', flexDirection: 'row', padding: 10 }}>
          <Image style={{width: 60, height: 60, marginRight: 10}} source={require('./img/job-icon.png')} />
          <View style={{width: 230, flexDirection: 'column'}}>
            <Text style={{fontSize: 16, color: '#74A2D8'}}>
            {news.tyotehtava}
            </Text>
            <Text>
              Haku päättyy: {news.haku_paattyy_pvm}
            </Text>
          </View>
        </View>
      );
    });
  }
    render() {
      var ScreenHeight = Dimensions.get("window").height - 75;

        return (
            <View style={{flex: 1}}>
              <SegmentedControlIOS
                values={['Healthcare', 'Teaching', 'All']}
                selectedIndex={this.state.selectedIndex}
                onChange={(event) => {
                  this.setState({
                    selectedIndex: event.nativeEvent.selectedSegmentIndex,
                  })
                }}
              />
              <ScrollView 
              automaticallyAdjustContentInsets={false}
              onScroll={() => {}}
              scrollEventThrottle={200}
              style={{height: ScreenHeight, marginBottom: 50}}>
              {this.renderAllCollectionsListView()}
              </ScrollView>
            </View>
        );
    }
};

export default JobListScene;
