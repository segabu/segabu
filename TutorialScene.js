'use strict';

import React, { Component } from 'react';
import Button from 'react-native-button';
import NavigationBar from 'react-native-navbar';
import { Avatar, Card, COLOR } from 'react-native-material-design';
import * as Animatable from 'react-native-animatable';
import * as Progress from 'react-native-progress';
import JobListScene from './JobListScene';
import GameScene from './GameScene';
import Badges from './Badges';
import SkillScene from './SkillScene';
import CVScene from './CVScene';
import ChatScene from './ChatScene';
import ProfilePage from './ProfilePage';
import BadgeScene from './BadgeScene';
import NotificationScene from './NotificationScene';
import LogoutScene from './LogoutScene';
import styles from './style';
import {
  AppRegistry,
  StyleSheet,
  AsyncStorage,
  Dimensions,
  TabBarIOS,
  Animated,
  Text,
  ListView,
  ScrollView,
  View,
  TextInput,
  Image,
  NavigatorIOS,
  TouchableHighlight
} from 'react-native';

function NotFoundError(e) {
    return e.statusCode === 404;
}

class TutorialScene  extends Component {
  constructor(props) {
    super(props);
    this.state = {
      notifCount: 1,
      rovers: [],
      selectedTab: 'blueTab',
      text: '',
      level: '',
      points: ''
    }
  }

  _handleBackPress() {
    this.props.navigator.pop();
  }
    componentDidMount() {
      this.setState({
        rovers: this.props.data,
        level: this.props.data.level,
        points: this.props.data.points
      })
    }
    onRightButtonPress() {
        this.refs.nav.push({
            title: 'Chat',
            tintColor: "#fff",
            titleTextColor: "#fff",
            barTintColor: '#9b59b6',
            component: ChatScene
        })
    }
    onLeftButtonPress() {
        this.refs.nav.push({
            title: 'Login2',
            component: HomeScene2
        })
    }

    render() {
      return (
        <Image source={require('./img/Background-01.png')} style={styles.backgroundImage}>
        <View style={{paddingTop: 20}}>
        <Text style={{marginBottom: 5, backgroundColor: 'transparent', color: 'white', textAlign: 'center', fontSize: 18}}>Please answer the following</Text>
        <Text style={{marginBottom: 5, backgroundColor: 'transparent', color: 'white', textAlign: 'center', fontSize: 18}}>quetions to set up</Text>
        <Text style={{marginBottom: 20, backgroundColor: 'transparent', color: 'white', textAlign: 'center', fontSize: 18}}>your account new one</Text>
          <View style={{alignItems: 'center'}}>
            <Text style={{backgroundColor: 'transparent', color: '#fff', fontSize: 18}}>I am</Text>
            <View style={{paddingTop: 10, paddingBottom: 50, flexWrap: 'wrap', alignItems: 'flex-start', flexDirection:'row', paddingLeft: 10}}>
            <Button 
              containerStyle={{padding:10, paddingLeft: 18, marginTop: 5, height: 70, width: 70, overflow:'hidden', borderRadius: 35, borderWidth: 3, borderColor: 'white', backgroundColor: 'transparent'}}
              styleDisabled={{color: 'blue'}}
              onPress={() => this.Cv()}>
              <Text style={{paddingTop: 12, marginLeft: -5, fontSize: 14, color: 'white'}}>Male</Text>
            </Button>
            <Button 
              containerStyle={{marginLeft: 20, paddingLeft: 12, marginRight: 20, padding:10, marginTop: 5, height: 70, width: 70, overflow:'hidden', borderRadius: 35, borderWidth: 3, borderColor: 'white', backgroundColor: 'transparent'}}
              styleDisabled={{color: 'blue'}}
              onPress={() => this.Cv()}>
              <Text style={{paddingTop: 12, marginLeft: -5, fontSize: 14, color: 'white'}}>Female</Text>
            </Button>
            <Button 
              containerStyle={{padding:10, paddingLeft: 15, marginTop: 5, height: 70, width: 70, overflow:'hidden', borderRadius: 35, borderWidth: 3, borderColor: 'white', backgroundColor: 'transparent'}}
              styleDisabled={{color: 'blue'}}
              onPress={() => this.Cv()}>
              <Text style={{paddingTop: 12, marginLeft: -5, fontSize: 14, color: 'white'}}>Other</Text>
            </Button>
            </View>
            <Text style={{backgroundColor: 'transparent', color: '#fff', fontSize: 18}}>My age is between</Text>
            <View style={{paddingTop: 10, paddingBottom: 50, flexWrap: 'wrap', alignItems: 'flex-start', flexDirection:'row', paddingLeft: 10}}>
            <Button 
              containerStyle={{padding:10, paddingLeft: 15,  marginTop: 5, height: 70, width: 70, overflow:'hidden', borderRadius: 35, borderWidth: 3, borderColor: 'white', backgroundColor: 'transparent'}}
              styleDisabled={{color: 'blue'}}
              onPress={() => this.Cv()}>
              <Text style={{paddingTop: 12, marginLeft: -5, fontSize: 14, color: 'white'}}>18-29</Text>
            </Button>
            <Button 
              containerStyle={{marginLeft: 20, paddingLeft: 15, marginRight: 20, padding:10, marginTop: 5, height: 70, width: 70, overflow:'hidden', borderRadius: 35, borderWidth: 3, borderColor: 'white', backgroundColor: 'transparent'}}
              styleDisabled={{color: 'blue'}}
              onPress={() => this.Cv()}>
              <Text style={{paddingTop: 12, marginLeft: -5, fontSize: 14, color: 'white'}}>30-47</Text>
            </Button>
            <Button 
              containerStyle={{padding:10, paddingLeft: 15, marginTop: 5, height: 70, width: 70, overflow:'hidden', borderRadius: 35, borderWidth: 3, borderColor: 'white', backgroundColor: 'transparent'}}
              styleDisabled={{color: 'blue'}}
              onPress={() => this.Cv()}>
              <Text style={{paddingTop: 12, marginLeft: -5, fontSize: 14, color: 'white'}}>48-65</Text>
            </Button>
            </View>
          </View>
        </View>
        </Image>
      );
    }
};

export default TutorialScene;
