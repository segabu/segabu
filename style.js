'use strict';

var React = require('react-native');

var {
    StyleSheet,
} = React;

module.exports = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    scene: {
        //padding: 10,
        paddingTop: 64,
        flex: 1,
        backgroundColor: 'transparent',
    },
    sceneProfile: {
        paddingTop: 64,
        flex: 1,
        backgroundColor: '#fff',
    },
    list: {
        justifyContent: 'center',
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
    row: {
        justifyContent: 'center',
        padding: 5,
        margin: 10,
        width: 350,
        height: 100,
        backgroundColor: '#F6F6F6',
        alignItems: 'center',
        borderWidth: 1,
        borderRadius: 5,
        borderColor: '#CCC'
    },
    thumb: {
        width: 64,
        height: 64
    },
    text: {
        flex: 1,
        marginTop: 5,
        fontWeight: 'bold'
    },
    backgroundImage: {
      flex: 1,
      resizeMode: 'cover', // or 'stretch'
      width: null,
      height: null,
    },
    page: {
     flex: 1
  }
});
