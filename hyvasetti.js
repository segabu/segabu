'use strict';

import React, { Component } from 'react';
import Button from 'react-native-button';
import NavigationBar from 'react-native-navbar';
import { Avatar, Card, COLOR } from 'react-native-material-design';
import * as Animatable from 'react-native-animatable';
import ProgressBar from 'react-native-progress/Bar';
import Icon from 'react-native-vector-icons/FontAwesome';
//import Auth0Lock from 'react-native-lock';
import GameScene from './GameScene';
import SkillScene from './SkillScene';
import CVScene from './CVScene';
import ChatScene from './ChatScene';
import LogoutScene from './LogoutScene';
import styles from './style';
import {
  AppRegistry,
  StyleSheet,
  TabBarIOS,
  Text,
  ListView,
  ScrollView,
  View,
  TextInput,
  Image,
  NavigatorIOS,
  TouchableHighlight
} from 'react-native';


function NotFoundError(e) {
    return e.statusCode === 404;
}

class ForTouchScene extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rovers: [],
      salasana: '',
      email: '',
      lastName: '',
      firstName: ''
    }
  }
    /*componentDidMount() {
      var myInit = {
        method: 'GET'
      };
      fetch('https://segabu2.herokuapp.com/contacts', myInit)
          .then((result) => {
            return result.json();
          }).then((data) => {
              this.setState({
                rovers: data,
                salasana: data[2].password,
                email: data[2].email,
                lastName: data[2].lastName,
                firstName: data[2].firstName,
                id: data[2]._id
              })
          }).catch(NotFoundError, function(e) {
          reply(e);
      })
    }*/
    componentDidMount() {
      var myInit = {
        method: 'GET'
      };
      fetch('https://segabu2.herokuapp.com/contacts', myInit)
          .then((result) => {
            return result.json();
          }).then((data) => {
              //data.map(function(tester) {
                //if(tester.email == 'Joo') {
                  this.setState({
                    rovers: data,
                    salasana: data[0].password,
                    email: data[0].email,
                    lastName: data[0].lastName,
                    firstName: data[0].firstName,
                    id: data[0]._id,
                    level: data[0].level
                  })
                //}
              //})
          }).catch(NotFoundError, function(e) {
          reply(e);
      })
    }
    render() {
    /*var Level = this.state.rovers.map(function(comment) {
      return (
        <Text key={comment._id}>
          //{comment.level}
        </Text>
      );
    });

    var Email = this.state.rovers.map(function(comment) {
      return (
        <Text key={comment._id}>
          {comment.email}
        </Text>
      );
    });
    */
    var ID = this.state.rovers.map(function(comment) {
      //if(comment.email == 'Joo') {
        return (
          <Text key={comment._id}>
              {comment._id}
          </Text>
        );
      //};
    });
   // var FullName = this.state.rovers.map(function(comment) {
      //if(comment.email == 'Joo') {
     //   return (
      //    <Text key={comment._id}>
      //      {comment.firstName} {comment.lastName}
      //    </Text>
      //  );
      //}
    //});
    var Level = this.state.level;
    var FullName = this.state.firstName + ' ' +this.state.lastName;
    var Email = this.state.email;
        return (
            <View style={styles.sceneProfile}>
              <View style={{height: 200, backgroundColor: "#03C9A9", alignItems: 'center'}}>
                <Image
                  style={{width: 80, height: 80, borderRadius: 40, marginTop: 50}}
                  source={require('./img/naamake.png')}
                />
              <Text style={{color: '#fff', marginTop: 10}}>Level {Level}</Text>
              </View>
              <Text style={{color: '#fff', marginTop: 10, marginLeft: 10}}>Profile</Text>
                  <Card>
                      <Card.Body>
                          <Text>Name: {FullName}</Text>
                      </Card.Body>
                  </Card>
                  <Card>
                      <Card.Body>
                          <Text>Email: {Email}</Text>
                      </Card.Body>
                  </Card>
                  <Card>
                      <Card.Body>
                          <Text>Hobbies: Computers, Movies</Text>
                      </Card.Body>
                  </Card>
                  <Card>
                      <Card.Body>
                          <Text>LinkedIn: www.linkedin.com</Text>
                      </Card.Body>
                  </Card>
                  <Card>
                      <Card.Body>
                          <Text>City: Oulu</Text>
                      </Card.Body>
                  </Card>
            </View>
        );
    }
};

class HomeScene  extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rovers: [],
      text: ''
    }
  }
    Profile() {
        this.props.navigator.push({
            title: 'Profile',
            tintColor: "#fff",
            backButtonTitle: 'Back',
            titleTextColor: "#fff",
            barTintColor: '#03C9A9',
            component: ForTouchScene
        });
    }
    CV() {
        this.props.navigator.push({
            tintColor: "#fff",
            titleTextColor: "#fff",
            barTintColor: '#262626',
            title: 'CV',
            component: CVScene
        });
    }
    Skills() {
        this.props.navigator.push({
            tintColor: "#fff",
            titleTextColor: "#fff",
            barTintColor: '#262626',
            title: 'Skills',
            component: SkillScene
        });
    }
    Games() {
        this.props.navigator.push({
            tintColor: "#fff",
            titleTextColor: "#fff",
            barTintColor: '#262626',
            title: 'Games',
            component: GameScene
        });
    }
    componentDidMount() {
      var myInit = {
        method: 'GET'
      };
      //fetch('http://localhost:1337/api/products', myInit)
      fetch('https://segabu2.herokuapp.com/contacts', myInit)
          .then((result) => {
            return result.json();
          }).then((data) => {
              this.setState({
                rovers: data
              })
          }).catch(NotFoundError, function(e) {
          reply(e);
      })
    }

    onRightButtonPress() {
        this.refs.nav.push({
            title: 'Chat',
            tintColor: "#fff",
            titleTextColor: "#fff",
            barTintColor: '#262626',
            component: ChatScene
        })
    }
    onLeftButtonPress() {
        this.refs.nav.push({
            title: 'Login2',
            component: HomeScene2
        })
    }

    render() {
      var commentNodes = this.state.rovers.map(function(comment) {
        return (
          <Text key={comment._id}>
            {comment.level}
          </Text>
        );
      });
      return (
          <View style={[styles.scene, {backgroundColor: '#262626', height: 150, alignItems: 'center'}]}>
            <View>
              <Image
                style={{width: 80, height: 80, borderRadius: 40, marginTop: 20}}
                source={require('./img/naamake.png')}
              />
            <Text style={{color: '#fff', marginTop: 10}}>Level {commentNodes}</Text>
            </View>
            <Button 
              containerStyle={{padding:10, marginTop: 20, height:45, width: 200, overflow:'hidden', borderRadius:4, backgroundColor: 'green'}}
              style={{fontSize: 20, color: 'white'}}
              styleDisabled={{color: 'blue'}}
              onPress={() => this.Profile()}>
              Profile
            </Button>
            <Button 
              containerStyle={{padding:10, marginTop: 5, height:45, width: 200, overflow:'hidden', borderRadius:4, backgroundColor: 'green'}}
              style={{fontSize: 20, color: 'white'}}
              styleDisabled={{color: 'blue'}}
              onPress={() => this.CV()}>
              CV
            </Button>
            <Button 
              containerStyle={{padding:10, marginTop: 5, height:45, width: 200, overflow:'hidden', borderRadius:4, backgroundColor: 'green'}}
              style={{fontSize: 20, color: 'white'}}
              styleDisabled={{color: 'blue'}}
              onPress={() => this.Skills()}>
              Skills
            </Button>
            <Button 
              containerStyle={{padding:10, marginTop: 5, height:45, width: 200, overflow:'hidden', borderRadius:4, backgroundColor: 'green'}}
              style={{fontSize: 20, color: 'white'}}
              styleDisabled={{color: 'blue'}}
              onPress={() => this.Games()}>
              Games
            </Button>
          </View>
      );
    }
};

var SeGaBu = React.createClass({
    onRightButtonPress: function() {
        this.refs.nav.push({
            title: 'Chat',
            tintColor: "#fff",
            titleTextColor: "#fff",
            barTintColor: '#262626',
            component: ChatScene
        })
    },
    onLeftButtonPress: function() {
        this.refs.nav.push({
            title: 'Login2',
            component: HomeScene2
        })
    },
    render () {
        return (
            <NavigatorIOS ref="nav" style={styles.container} initialRoute={{
                component: HomeScene,
                title: 'SeGaBu Jobs',
                tintColor: "#fff",
                titleTextColor: "#fff",
                barTintColor: '#262626',
                rightButtonTitle: 'Chat',
                onRightButtonPress: this.onRightButtonPress,
                onLeftButtonPress: this.onLeftButtonPress
            }} />
        );
    }
});


export default HomeScene;
