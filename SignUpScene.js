'use strict';

import React, { Component } from 'react';
import Button from 'react-native-button';
import NavigationBar from 'react-native-navbar';
import reactRouter from 'react-router';
import { Router, Route, Link } from 'react-router';
import styles from './style';
import {
  AppRegistry,
  StyleSheet,
  TabBarIOS,
  Text,
  ListView,
  ScrollView,
  View,
  TextInput,
  Image,
  NavigatorIOS,
  TouchableHighlight
} from 'react-native';

class SignUpScene extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: '',
      lastName: '',
      userName: '',
      textEmail: '',
      textPW: '',
    }
  }

AddUser() {
  fetch('https://segabu2.herokuapp.com/contacts/', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      firstName: this.state.firstName,
      userName: this.state.userName,
      lastName: this.state.lastName,
      email: this.state.textEmail,
      password: this.state.textPW,
      points: 1,
      level: 1,
      passwordConfirm: this.state.textPWconfirm
    })
  })
  this.handleBackPress();
}
handleBackPress() {
  this.props.navigator.pop();
}


UpdateUser() {
  fetch('https://segabu2.herokuapp.com/contacts/584816028536e100111dcdc7', {
    method: 'PUT',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      firstName: "",
      userName: "Rupsu",
      lastName: "Sukunimi",
      email: "",
      password: "sala",
      points: 4001,
      level: 12,
      passwordConfirm: "sala"
    })
  })
  this.handleBackPress();
}
handleBackPress() {
  this.props.navigator.pop();
}

  render() {
    return (
      <Image source={require('./img/Background-01.png')} style={styles.backgroundImage}>
        <View style={[styles.scene]}>
          <View style={{alignItems: 'center'}}>
            <Text style={{color: '#fff', marginBottom: 5, fontSize: 20}}>Please choose a</Text>
            <Text style={{color: '#fff', marginBottom: 5, fontSize: 20}}>username and a</Text>
            <Text style={{color: '#fff', marginBottom: 5, fontSize: 20}}>password</Text>
          </View>
          <View style={{marginLeft: 40, marginRight: 40, marginTop: 30, borderColor: 'white', borderWidth: 1, borderRadius: 5}}>
            <View style={{borderBottomWidth: 1, borderColor: 'white'}}>
              <TextInput placeholder="Username" placeholderTextColor="white" style={{height: 35, color: 'white', paddingLeft: 10}} onChangeText={(userName) => this.setState({userName})}
              value={this.state.userName}/>
            </View>
            <View style={{borderBottomWidth: 1, borderColor: 'white'}}>
              <TextInput placeholder="Password" placeholderTextColor="white" secureTextEntry={true} style={{height: 35, color: 'white', paddingLeft: 10}} onChangeText={(textPW) => this.setState({textPW})}
              value={this.state.textPW}/>
            </View>
            <TextInput placeholder="Confirm password" placeholderTextColor="white" secureTextEntry={true} style={{height: 35, color: 'white', paddingLeft: 10}} onChangeText={(textPWconfirm) => this.setState({textPWconfirm})}
            value={this.state.textPWconfirm}/>
          </View>
          <View style={[styles.scene, {alignItems: 'center'}]}>
            <Button 
              containerStyle={{padding:10, marginTop: 5, height: 100, width: 100, overflow:'hidden', borderRadius: 70, borderWidth: 3, borderColor: 'white', backgroundColor: 'transparent'}}
              style={{fontSize: 20, color: 'white'}}
              onPress={() => this.AddUser()}>
              <Text style={{paddingTop: 22, paddingLeft: 5, marginLeft: -5, fontSize: 20, color: 'white'}}>Confirm</Text>
            </Button>
          </View>
        </View>
      </Image>
    );
  }
}
export default SignUpScene;
