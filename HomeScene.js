'use strict';

import React, { Component } from 'react';
import Button from 'react-native-button';
import NavigationBar from 'react-native-navbar';
import { Avatar, Card, COLOR } from 'react-native-material-design';
import * as Animatable from 'react-native-animatable';
import * as Progress from 'react-native-progress';
import JobListScene from './JobListScene';
import GameScene from './GameScene';
import Badges from './Badges';
import TutorialScene from './TutorialScene';
import SkillScene from './SkillScene';
import CVScene from './CVScene';
import ChatScene from './ChatScene';
import ProfilePage from './ProfilePage';
import BadgeScene from './BadgeScene';
import NotificationScene from './NotificationScene';
import LogoutScene from './LogoutScene';
import styles from './style';
import {
  AppRegistry,
  StyleSheet,
  AsyncStorage,
  Dimensions,
  TabBarIOS,
  Animated,
  Text,
  ListView,
  ScrollView,
  View,
  TextInput,
  Image,
  NavigatorIOS,
  TouchableHighlight
} from 'react-native';

function NotFoundError(e) {
    return e.statusCode === 404;
}

class HomeScene  extends Component {
  constructor(props) {
    super(props);
    this.state = {
      notifCount: 1,
      rovers: [],
      selectedTab: 'blueTab',
      text: '',
      level: '',
      points: ''
    }
  }

  _handleBackPress() {
    this.props.navigator.pop();
  }
    componentDidMount() {
      this.setState({
        rovers: this.props.data,
        level: this.props.data.level,
        points: this.props.data.points
      })
    }
    onRightButtonPress() {
        this.refs.nav.push({
            title: 'Chat',
            tintColor: "#fff",
            titleTextColor: "#fff",
            barTintColor: '#9b59b6',
            component: ChatScene
        })
    }
    onLeftButtonPress() {
        this.refs.nav.push({
            title: 'Login2',
            component: HomeScene2
        })
    }
    _renderContent = (color: string, pageText: string, num?: number) => {
      return (
        <View style={[styles.tabContent, {backgroundColor: '#fff'}]}>
          <Text style={styles.tabText}>{pageText}</Text>
          <Text style={styles.tabText}>{num} re-renders of the {pageText}</Text>
        </View>
      );
    };

    render() {
      return (
        <View style={[styles.scene]}>
          <TabBarIOS
            tintColor="#fff"
            barTintColor="rgb(176, 206, 216)"
            >
            <TabBarIOS.Item
              title=""
              icon={require('./img/iconProfile.png')}
              renderAsOriginal
              selected={this.state.selectedTab === 'blueTab'}
              onPress={() => {
                this.setState({
                  selectedTab: 'blueTab'
                });
              }}>
              <ProfilePage navigator={this.props.navigator} commentNodes = {this.state.level} points = {this.state.points} />
            </TabBarIOS.Item>
            <TabBarIOS.Item
              title=""
              icon={require('./img/iconChat.png')}
              renderAsOriginal
              selected={this.state.selectedTab === 'redTab'}
              onPress={() => {
                this.setState({
                  selectedTab: 'redTab'
                });
              }}>
              <ChatScene />
            </TabBarIOS.Item>
            <TabBarIOS.Item
              icon={require('./img/iconBrain.png')}
              renderAsOriginal
              title=""
              selected={this.state.selectedTab === 'greenTab'}
              onPress={() => {
                this.setState({
                  selectedTab: 'greenTab'
                });
              }}>
              <SkillScene />
            </TabBarIOS.Item>
            <TabBarIOS.Item
              icon={require('./img/iconWork.png')}
              renderAsOriginal
              title=""
              selected={this.state.selectedTab === 'yellowTab'}
              onPress={() => {
                this.setState({
                  selectedTab: 'yellowTab'
                });
              }}>
              <JobListScene />
            </TabBarIOS.Item>
            <TabBarIOS.Item
              icon={require('./img/iconNotifications.png')}
              badge={this.state.notifCount > 0 ? this.state.notifCount : undefined}
              renderAsOriginal
              title=""
              selected={this.state.selectedTab === 'purpleTab'}
              onPress={() => {
                this.setState({
                  selectedTab: 'purpleTab'
                });
              }}>
              <NotificationScene />
            </TabBarIOS.Item>
          </TabBarIOS>
        </View>
      );
    }
};

export default HomeScene;
