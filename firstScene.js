'use strict';

import React, { Component } from 'react';
import Button from 'react-native-button';
import NavigationBar from 'react-native-navbar';
import { Avatar, Card, COLOR } from 'react-native-material-design';
import * as Animatable from 'react-native-animatable';
import * as Progress from 'react-native-progress';
import Icon from 'react-native-vector-icons/FontAwesome';
import GameScene from './GameScene';
import SkillScene from './SkillScene';
import CVScene from './CVScene';
import ChatScene from './ChatScene';
import BadgeScene from './BadgeScene';
import LogoutScene from './LogoutScene';
import styles from './style';
import {
  AppRegistry,
  StyleSheet,
  AsyncStorage,
  Dimensions,
  TabBarIOS,
  Animated,
  Text,
  ListView,
  ScrollView,
  View,
  TextInput,
  Image,
  NavigatorIOS,
  TouchableHighlight
} from 'react-native';


class firstScene  extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rovers: [],
      selectedTab: 'blueTab',
      text: '',
    }
  }

render() {
      var ScreenHeight = Dimensions.get("window").height - 75;
      //var commentNodes = this.state.rovers.level;
      return (
        <View>
        <ScrollView 
        automaticallyAdjustContentInsets={false}
        onScroll={() => { console.log('onScroll!'); }}
        scrollEventThrottle={200}
        style={{marginTop: -10, height: ScreenHeight, marginBottom: -75}}>
          <View style={{backgroundColor: '#A870BF', height: 150, alignItems: 'center'}}>
              <View>
                <Image
                  style={{width: 80, height: 80, borderRadius: 40, marginTop: 20}}
                  source={require('./img/naamake.png')}
                />
              <Text className="Leveli" style={{color: '#fff', marginTop: 10, textAlign: 'center'}}>Level {commentNodes}</Text>
              </View>
          </View>
          <Text style={{paddingLeft: 10, paddingTop: 10}}>Progress</Text>
          <View style={{borderBottomWidth: 0.2, marginTop: 20, alignItems: 'center', paddingBottom: 20}}>
              <Progress.Bar color={'#65C6BB'} height={10} borderRadius={10} progress={0.3} width={200} />
          </View>
          <TouchableHighlight onPress={()=> this.Badges()} >
          <View>
            <Text style={{paddingLeft: 10, paddingTop: 10}}>Badges</Text>
            <View style={{borderBottomWidth: 0.2, marginTop: 20, paddingBottom: 20, flexWrap: 'wrap', alignItems: 'flex-start', flexDirection:'row', paddingLeft: 10}}>
                  <Image
                    style={{width: 40, height: 40, marginRight: 10}}
                    source={require('./img/analysis.png')}
                  />
                  <Image
                    style={{width: 40, height: 40, marginRight: 10}}
                    source={require('./img/target.png')}
                  />
                  <Image
                    style={{width: 40, height: 40, marginRight: 10}}
                    source={require('./img/testing.png')}
                  />
                  <Image
                    style={{width: 40, height: 40, marginRight: 10}}
                    source={require('./img/chronometer.png')}
                  />
                  <Image
                    style={{width: 40, height: 40, marginRight: 10}}
                    source={require('./img/html.png')}
                  />
                  <Image
                    style={{width: 40, height: 40}}
                    source={require('./img/quality.png')}
                  />
            </View>
          </View>
          </TouchableHighlight>
        </ScrollView>
        </View>
        )};
};

export default firstScene;
