'use strict';

import React, { Component } from 'react';
import Button from 'react-native-button';
import NavigationBar from 'react-native-navbar';
import * as Animatable from 'react-native-animatable';
import * as Progress from 'react-native-progress';
import LogoutScene from './LogoutScene';
import styles from './style';
import {
  AppRegistry,
  StyleSheet,
  TabBarIOS,
  Text,
  ListView,
  ScrollView,
  View,
  TextInput,
  Image,
  NavigatorIOS,
  TouchableHighlight
} from 'react-native';

class Badges extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      //selectedTab: 'redTab'
    }
  }
render() {
	return (
		<View style={{backgroundColor: '#b0ced8', borderBottomWidth: 0.2, paddingTop: 10, paddingBottom: 20, flexWrap: 'wrap', alignItems: 'flex-start', flexDirection:'row', paddingLeft: 10}}>
		    <Image
		      style={{width: 40, height: 40, marginRight: 10}}
		      source={require('./img/chronometer.png')}
		    />
		    <Image
		      style={{width: 40, height: 40, marginRight: 10}}
		      source={require('./img/html.png')}
		    />
		    <Image
		      style={{width: 40, height: 40, marginRight: 10}}
		      source={require('./img/quality.png')}
		    />
		    <Image
		      style={{width: 40, height: 40, marginRight: 10}}
		      source={require('./img/support.png')}
		    />
		    <Image
		      style={{width: 40, height: 40, marginRight: 10}}
		      source={require('./img/vector.png')}
		    />
		    <Image
		      style={{width: 40, height: 40, marginRight: 10}}
		      source={require('./img/photo-camera.png')}
		    />
		    <Image
		      style={{width: 40, height: 40}}
		      source={require('./img/attach.png')}
		    />
		</View>
	)};
};
export default Badges;
