'use strict';

import React, { Component } from 'react';
import Button from 'react-native-button';
import NavigationBar from 'react-native-navbar';
import * as Animatable from 'react-native-animatable';
import * as Progress from 'react-native-progress';
import LogoutScene from './LogoutScene';
import Badges from './Badges';
import BadgeScene from './BadgeScene';
import styles from './style';
import {
  AppRegistry,
  StyleSheet,
  TabBarIOS,
  Text,
  ListView,
  ScrollView,
  View,
  TextInput,
  Image,
  Dimensions,
  NavigatorIOS,
  TouchableHighlight
} from 'react-native';

class ProfilePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      //selectedTab: 'redTab'
    }
  }
  _handleBackPress() {
    this.props.navigator.pop();
  }
  BadgesFunction() {
      this.props.navigator.push({
          leftButtonTitle: '<-',
          tintColor: '#fff',
          titleTextColor: '#fff',
          barTintColor: '#BAD4DC',
          title: 'Badges',
          shadowHidden: true,
          onLeftButtonPress: () => this._handleBackPress(),
          component: BadgeScene
      }); 
  }
render() {
  var ScreenHeight = Dimensions.get("window").height - 75;
  return (
    <View style={{marginTop: 10}}>
      <ScrollView 
      automaticallyAdjustContentInsets={false}
      onScroll={() => {}}
      scrollEventThrottle={200}
      style={{marginTop: -10, height: ScreenHeight, marginBottom: -75}}>
        <View style={{backgroundColor: '#BAD4DC', height: 175, alignItems: 'center'}}>
            <View>
              <Image
                style={{width: 80, height: 80, borderRadius: 40, marginTop: 20}}
                source={require('./img/profileavatar.png')}
              />
            <Text style={{color: '#fff', marginTop: 5, textAlign: 'center', fontWeight: '600'}}>Level {this.props.commentNodes}</Text>
            <Text style={{color: '#fff', marginTop: 5, textAlign: 'center', fontWeight: '600'}}>Points {this.props.points}</Text>            
            </View>
        </View>
        <Text style={{paddingLeft: 10, paddingTop: 10, color: 'rgb(176, 206, 216)', fontWeight: '600'}}>Experience</Text>
        <View style={{borderBottomWidth: 0.2, marginTop: 30, alignItems: 'center', paddingBottom: 40}}>
            <Progress.Bar color={'rgb(176, 206, 216)'} height={17} borderRadius={10} progress={0.8} width={300} />
        </View>
        <TouchableHighlight onPress={()=> this.BadgesFunction()} >
        <View> 
          <Text style={{paddingLeft: 10, paddingTop: 10, fontWeight: '600', color: '#fff', backgroundColor: '#b0ced8'}}>Badges</Text>
          <Badges />
        </View>
        </TouchableHighlight>
        <Text style={{paddingLeft: 10, paddingTop: 10, color: '#C3D9E0', fontWeight: '600'}}>Previously played</Text>
        <View style={{borderBottomWidth: 0.2, paddingTop: 10, paddingBottom: 70, flexWrap: 'wrap', alignItems: 'flex-start', flexDirection:'row', paddingLeft: 10}}>
            <Image
              style={{width: 40, height: 40, marginRight: 10}}
              source={require('./img/1.png')}
            />
            <Image
              style={{width: 40, height: 40, marginRight: 10}}
              source={require('./img/2.png')}
            />
            <Image
              style={{width: 40, height: 40, marginRight: 10}}
              source={require('./img/3.png')}
            />
            <Image
              style={{width: 40, height: 40, marginRight: 10}}
              source={require('./img/4.png')}
            />
            <Image
              style={{width: 40, height: 40, marginRight: 10}}
              source={require('./img/5.png')}
            />
            <Image
              style={{width: 40, height: 40, marginRight: 10}}
              source={require('./img/7.png')}
            />
        </View>
      </ScrollView>
    </View>
  )};
};
export default ProfilePage;