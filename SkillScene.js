'use strict';

import React, { Component } from 'react';
import Button from 'react-native-button';
import NavigationBar from 'react-native-navbar';
import reactRouter from 'react-router';
import { Router, Route, Link } from 'react-router';
import GameScene from './GameScene';
import HomeScene from './HomeScene';
import CVScene from './CVScene';
import ChatScene from './ChatScene';
import LogoutScene from './LogoutScene';
import styles from './style';
import {
  AppRegistry,
  StyleSheet,
  TabBarIOS,
  Text,
  ListView,
  ScrollView,
  View,
  TextInput,
  Image,
  NavigatorIOS,
  SegmentedControlIOS,
  TouchableHighlight
} from 'react-native';

class SkillScene extends Component {
  constructor(props) {
    super(props);
    this.state = {
        selectedTab: 'redTab',
    }
  }

    render() {
        return (
            <View>
              <SegmentedControlIOS
                values={['IT', 'Marketing', 'Other']}
                style={{marginLeft: 10, marginRight: 10, marginTop: 10, marginBottom: 10}}
                selectedIndex={this.state.selectedIndex}
                onChange={(event) => {
                  this.setState({selectedIndex: event.nativeEvent.selectedSegmentIndex});
                }}
              />
            <Text style={{paddingLeft:10, paddingTop: 10}}>Recommended</Text>
              <View style={{borderBottomWidth: 0.2, paddingTop: 10, paddingBottom: 70, flexWrap: 'wrap', alignItems: 'flex-start', flexDirection:'row', paddingLeft: 10}}>
                <Image
                  style={{width: 70, height: 100, marginRight: 10}}
                  source={require('./img/csharp.png')}
                />
                <Image
                  style={{width: 70, height: 100, marginRight: 10}}
                  source={require('./img/interviewer.png')}
                />
                <Image
                  style={{width: 70, height: 100, marginRight: 10}}
                  source={require('./img/chemy.png')}
                />
            </View>
            <Text style={{paddingLeft:10, paddingTop: 10}}>Popular</Text>
              <View style={{borderBottomWidth: 0.2, paddingTop: 10, paddingBottom: 70, flexWrap: 'wrap', alignItems: 'flex-start', flexDirection:'row', paddingLeft: 10}}>
                <Image
                  style={{width: 70, height: 100, marginRight: 10}}
                  source={require('./img/csharp.png')}
                />
                <Image
                  style={{width: 70, height: 100, marginRight: 10}}
                  source={require('./img/interviewer.png')}
                />
                <Image
                  style={{width: 70, height: 100, marginRight: 10}}
                  source={require('./img/sql.png')}
                />
              </View>
            </View>
        );
    }
};

export default SkillScene;
