'use strict';

import React, { Component } from 'react';
import Button from 'react-native-button';
import NavigationBar from 'react-native-navbar';
import reactRouter from 'react-router';
import { Router, Route, Link } from 'react-router';
import GameScene from './GameScene';
import HomeScene from './HomeScene';
import Modal from 'react-native-simple-modal';
import CVScene from './CVScene';
import styles from './style';
import {
  AppRegistry,
  StyleSheet,
  Text,
  ListView,
  Dimensions,
  ScrollView,
  View,
  TextInput,
  Image,
  NavigatorIOS,
  TouchableOpacity,
  TouchableHighlight
} from 'react-native';

class BadgeScene extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      selectedTab: 'redTab'
    }
  }
    render() {
      var ScreenHeight = Dimensions.get("window").height - 75;
        return (
            <View style={styles.scene}>
              <ScrollView 
              automaticallyAdjustContentInsets={false}
              onScroll={() => { console.log('onScroll!'); }}
              scrollEventThrottle={200}
              style={{marginTop: -10, height: ScreenHeight, marginBottom: -75}}>
                <View style={{backgroundColor: '#BAD4DC', height: 100, alignItems: 'center'}}>
                    <View style={{flexWrap: 'wrap', alignItems: 'flex-start', flexDirection:'row'}}>
                      <View>
                        <Image
                          style={{width: 80, height: 80, borderRadius: 40, marginTop: 10, marginLeft: -20}}
                          source={require('./img/medal2.png')}
                        />
                      </View>
                      <View style={{width: 220, paddingTop: 15}}>
                        <Text style={{color: '#fff', marginTop: 10}}>You have unlocked 3 of 10 collective badges.</Text>
                      </View>
                    </View>
                </View>
                <View>
                  <View style={{marginTop: 20, paddingBottom: 20, flexWrap: 'wrap', alignItems: 'flex-start', flexDirection:'row', paddingLeft: 10}}>
                    <TouchableOpacity onPress={() => this.setState({open: true})}>
                      <Image
                        style={{width: 40, height: 40, marginRight: 10, marginBottom: 10}}
                        source={require('./img/analysis.png')}
                      />
                    </TouchableOpacity>
                    <Image
                      style={{width: 40, height: 40, marginRight: 10, marginBottom: 10}}
                      source={require('./img/chronometer.png')}
                    />
                    <Image
                      style={{width: 40, height: 40, marginRight: 10, marginBottom: 10}}
                      source={require('./img/html.png')}
                    />
                    <Image
                      style={{width: 40, height: 40, marginRight: 10, marginBottom: 10}}
                      source={require('./img/support.png')}
                    />
                    <Image
                      style={{width: 40, height: 40, marginRight: 10, marginBottom: 10}}
                      source={require('./img/vector.png')}
                    />
                    <Image
                      style={{width: 40, height: 40, marginRight: 10, marginBottom: 10}}
                      source={require('./img/photo-camera.png')}
                    />
                    <Image
                      style={{width: 40, height: 40, marginRight: 10, marginBottom: 10}}
                      source={require('./img/attach.png')}
                    />
                    <Image
                      style={{width: 40, height: 40, marginRight: 10, marginBottom: 10}}
                      source={require('./img/vector.png')}
                    />
                    <Image
                      style={{width: 40, height: 40, marginRight: 10, marginBottom: 10}}
                      source={require('./img/photo-camera.png')}
                    />
                    <Image
                      style={{width: 40, height: 40}}
                      source={require('./img/attach.png')}
                    />
                  </View>
                </View>
                <Modal
                   offset={this.state.offset}
                   open={this.state.open}
                   modalDidOpen={() => console.log('modal did open')}
                   modalDidClose={() => this.setState({open: false})}
                   style={{alignItems: 'center', marginTop: -10, height: ScreenHeight, marginBottom: -75}}>
                   <View>
                      <Image
                        style={{width: 40, height: 40, marginRight: 10}}
                        source={require('./img/analysis.png')}
                      />
                      <Text style={{fontSize: 20, marginBottom: 10}}>Cool badge</Text>
                      <TouchableOpacity
                         style={{margin: 5}}
                         onPress={() => this.setState({open: false})}>
                         <Text>Close modal</Text>
                      </TouchableOpacity>
                   </View>
                </Modal>
              </ScrollView>
            </View>
        );
    }
};

export default BadgeScene;
