'use strict';

import React, { Component } from 'react';
import Button from 'react-native-button';
import NavigationBar from 'react-native-navbar';
import reactRouter from 'react-router';
import { Router, Route, Link } from 'react-router';
import styles from './style';
import {
  AppRegistry,
  StyleSheet,
  TabBarIOS,
  Text,
  ListView,
  ScrollView,
  View,
  TextInput,
  Image,
  NavigatorIOS,
  TouchableHighlight
} from 'react-native';

class GameScene extends Component {
  constructor() {
    super();
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      dataSource: ds.cloneWithRows(['Game 1', 'Game 2','Game 3', 'Game 4','Game 5', 'Game 6','Game 7', 'Game 8']),
    };
  }

  render() {
    return (
      <ListView
        dataSource={this.state.dataSource}
        renderRow={(rowData) => <Text  style={styles.row}>{rowData}</Text>}/>
    );
  }
}

export default GameScene;
